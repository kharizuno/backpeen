import fs from 'fs';
import lib from '../../system/library';

class foundCtrl {

    static list(req, res) {
        let post = req.body;

        let rawdata = fs.readFileSync('./data/foundation.json');
        let dt = JSON.parse(rawdata);

        if (!req.session.sequence) req.session.sequence = lib.randInt(0, 9999);

        res.json(lib.reststats({'current_sequence_id': req.session.sequence, 'verification_hash': lib.md5en(req.session.sequence)}, dt));
    }

    static changes(req, res) {
        let post = req.body;

        let rawdata = fs.readFileSync('./data/foundation.json');
        let dt = JSON.parse(rawdata);

        const indexID = dt.findIndex(r => {
            return r.name === post.name;
        });

        let val = dt[indexID];
        let data = lib.extendify(val, {name: val.name, people_helped: parseFloat(post.people_helped)});

        fs.writeFileSync('./data/foundation.json', lib.stringify(dt));
        if (!req.session.sequence) req.session.sequence = lib.randInt(0, 9999);

        res.json(lib.reststats({'current_sequence_id': req.session.sequence, 'verification_hash': lib.md5en(req.session.sequence)}, data));
    }

}

export default foundCtrl;