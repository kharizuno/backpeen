import foundCtrl from '../controllers/foundCtrl';

const foundLink = (routes, multer, auth) => {
    // For Parsing multipart/form-data
    let upload = multer();

    routes.route('/api/v1/retrieve_all')
        .all(auth.token)
        .get(upload.array(), foundCtrl.list);

    routes.route('/api/v1/changes')
        .all(auth.token)
        .post(upload.array(), foundCtrl.changes);

};

export default foundLink;