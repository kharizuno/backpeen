import auth from '../middleware/auth';

import multer from 'multer';
import express from 'express';
const router = express.Router();

router.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header(
        'Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Content-Type, Accept'
    );
    next();
});

let test = {title: 'Peentar Test', desc: 'Untuk mengakses api gunakan headers berikut: <br><br> "X-Nixcenter-Token : NEEDMORE"'};
router.route('/')
    .get(function (req, res) {
        res.render('index', test);
    })
    .post(function (req, res) {
        res.render('index', test);
    });

import foundLink from './found';
foundLink(router, multer, auth);

export default router;