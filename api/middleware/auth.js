'use strict';

class auth {

    static token(req, res, next) {
        if (req.headers['x-nixcenter-token'] === 'NEEDMORE') {
            next();
            return true;
        } else {
            res.status(401);
            res.json({status: 'Error', message: 'Invalid Token'})
        }
    }

}

export default auth;