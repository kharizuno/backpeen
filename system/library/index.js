import extend from 'node.extend';
import cryptojs from 'crypto-js';
import encoding from 'text-encoding';
import serialize from 'serialize-javascript';
import * as wwvnn from './wordarray';

class library {

    static extendify(arr1, arr2) {
        return extend(arr1, arr2);
    }

    static stringify(dt) {
        return serialize(dt);
    }

    static md5en(dt) {
        return cryptojs.MD5(dt).toString();
    }

    static sha1en(dt, key = '') {
        let wordArr = cryptojs.HmacSHA1(serialize(dt), this.md5en(key));
        let utf8Arr = wwvnn.convertWordArrayToUint8Array(wordArr);
        let string = new encoding.TextDecoder('utf-8').decode(utf8Arr);
        return string;
    }

    static randInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    static reststats(opt = [], result, msg = 'Data is not available', comp) {
        let status = (result) ? 'Success' : 'Error';
        let message = (result) ? 'OK' : msg;
        if (comp) {
            message = msg;
        } else {
            let st = 1;
            switch (msg) {
                case 'Error':
                case 'Validation':
                    st = 0;
                    break;
            }

            if (st === 0 || comp) {
                status = 'Error';
                message = msg;
            }
        }

        let res = {status: status, message: message, data: (result) ? result : []};
        res = (opt) ? this.extendify(res, opt) : res;

        return res;
    }

}

export default library;